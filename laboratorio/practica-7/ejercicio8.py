def crearMatriz(M,orden):
  for i in range(orden):
    M.append([0]*orden)

def ingresar(M,orden):
  for i in range(orden):
    for j in range(orden):
      M[i][j]=int(input("Valor: "))

def mostrar(M,orden):
  for i in range(orden):
    for j in range(orden):
      print(M[i][j],end=" ")
    print()

def matrizIdentidad(M,orden):
  for i in range(orden):
    for j in range(orden):
      if i == j and M[i][j] != 1:
        return False
      if i != j and M[i][j] != 0:
        return False
  return True

M = []
orden = int(input("Orden de las matriz?: "))
crearMatriz(M,orden)
ingresar(M,orden)
mostrar(M,orden)

if matrizIdentidad(M,orden) == True:
  print("La matriz es identidad")
else:
  print("La matriz NO es identidad")





""" M=[]
f=int(input("Cantidad de filas? "))
c=int(input("Cantidad de columnas? "))
crearMatriz(M,f,c)
ingresar(M,f,c)
print("Matriz:")
mostrar(M,f,c,)
 """
