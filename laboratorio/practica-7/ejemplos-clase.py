def crearMatriz(M,n):
    for i in range(n):
        M.append([0]*n)
def ingresar(M,n):
    for i in range(n):
        for j in range(n):
            M[i][j]=int(input("Valor? "))
def mostrar(M,n):
    for i in range(n):
        for j in range(n):
            print(M[i][j],end="\t")
        print()

# 1. Calcular la suma de cada columna de una matriz.
def sumaCols(M,f,c):
    for i in range(c):
        s=0
        for j in range(f):
            s=s+M[j][i]
        print("Suma de Columna",i+1,"=",s)

# 2. Calcular el producto de los elementos de la diagonal principal de una matriz.
def productDiagPrinc(M,n):
    p=1
    for i in range(n):
        for j in range(n):
            if i==j:
                p=p*M[i][j]
    return p
# 5. Hallar la suma de la diagonal principal.

def sumaDiagPrinc(M,n):
    s=0
    for i in range(n):
        for j in range(n):
            if i==j:
                s=s+M[i][j]
    return s

# 5. Hallar la suma de la diagonal principal.

def sumaDiagPrinc2(M,n):
    s=0
    for i in range(n):
        s=s+M[i][i]
    return s

# 6. Hallar la suma de la diagonal secundaria.

def sumaDiagSec(M,n):
    s=0
    for i in range(n):
        s=s+M[i][n-1-i]
    return s

# 7. Sumar dos matrices de MxN.

def crearMatrizFxC(M,f,c):
    for i in range(f):
        M.append([0]*c)
def ingresarFxC(M,f,c):
    for i in range(f):
        for j in range(c):
            M[i][j]=int(input("Valor? "))
def mostrarFxC(M,f,c):
    for i in range(f):
        for j in range(c):
            print(M[i][j],end="\t")
        print()

def sumar(M1,M2,f,c):
    MS=[]
    crearMatrizFxC(MS,f,c)
    for i in range(f):
        for j in range(c):
            MS[i][j]=M1[i][j]+M2[i][j]
    mostrarFxC(MS,f,c)

""" M=[]
n=int(input("Orden de la matriz? "))
crearMatriz(M,n)
ingresar(M,n)
print("Matriz:")
mostrar(M,n)
sumaCols(M,n,n)
print("Producto de la Diagonal Principal=",productDiagPrinc(M,n))
print("Suma de la Diagonal Principal=",sumaDiagPrinc(M,n))
print("Suma de la Diagonal Principal=",sumaDiagPrinc2(M,n))
print("Suma de la Diagonal Secundaria=",sumaDiagSec(M,n))
 """
M1=[]
M2=[]
f=int(input("Cantidad de filas de la matriz? "))
c=int(input("Cantidad de columnas de la matriz? "))
crearMatrizFxC(M1,f,c)
crearMatrizFxC(M2,f,c)
print("Ingresando datos a M1:")
ingresarFxC(M1,f,c)
print("Ingresando datos a M2:")
ingresarFxC(M2,f,c)
print("Contenido M1:")
mostrarFxC(M1,f,c)
print("Contenido M2:")
mostrarFxC(M2,f,c)
print("Matriz Suma:")
sumar(M1,M2,f,c)