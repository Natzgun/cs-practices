def crearMatriz(M,f,c):
  for i in range(f):
    M.append([0]*c)

def ingresar(M,f,c):
  for i in range(f):
    for j in range(c):
      M[i][j]=int(input("Valor "))

def mostrar(M,f,c,):
  for i in range(f):
    for j in range(c):
      print('| ',M[i][j],end=" ")
    print('|')

## FUNCION PARA HALLAR LA SUMA DE LAS COLUMNAS
def sumaDeCol(M,f,c):
  for i in range(c):
    sum=0
    for j in range(f):
      sum+=M[j][i]
    print(f'la suma de la columa {i+1} es {sum}')

## FUNCION PARA HALLAR EL PRODUCTO DE LA DIAGONAL PRINCIPAL
def multDeDiag(M,f,c):
  mult = 1
  for i in range(f):
    for j in range(c):
      if i == j:
        mult = mult*M[j][i]
  return mult

## 5. SUMA DE LA DIAGONAL PRINCIPAL
def sumDiagP(M,f):
  mult = 0
  for i in range(f):
        mult = mult+M[i][i]
  return mult

## 6. SUMA DE LA DIAGONAL SECUNDARIA
def sumDiagS(M,f):
  sum = 0
  for i in range(f):
        sum = sum+M[i][f-1-i]
  return sum

## 7. SUMAR DOS MATRICES
def sumMatrices(M1,M2,f,c):
  Msu = []
  crearMatriz(Msu,f,c)  
  for i in range(f):
    for j in range(c):
      Msu[i][j] = M1[i][j]+M2[i][j]
  mostrar(Msu,f,c)
  


M=[]
M2 = []
f=int(input("Cantidad de filas? "))
c=int(input("Cantidad de columnas? "))

## Ingresar Matrices    
crearMatriz(M,f,c)
ingresar(M,f,c)
print('Ingresar la segunda Matriz')
crearMatriz(M2,f,c)
ingresar(M2,f,c)

## Mostrar Matrices
print("Matriz:")
mostrar(M,f,c,)
print('Mostrar la segunda matriz')
mostrar(M2,f,c)

""" ## Suma de las columnas
print('Sumas de las columnas')
sumaDeCol(M,f,c)

## Producto de la diagonal principal
print(f'El producto de la diagonal principal es:')
print(multDeDiag(M,f,c))

## Suma de la diagonal principal
print(f'La suma de la diagnoal secundaria es:')
print(sumDiagP(M,f))

## Suma de la diagonal secundaria
print(f'La suma de la diagnoal secundaria es:')
print(sumDiagS(M,f)) """

## Sumar 2 Matrices
print(f'La suma de las Matrices es:')
sumMatrices(M,M2,f,c)



