def crearMatriz(M,f,c):
  for i in range(f):
    M.append([0]*c)

def ingresar(M,f,c):
  for i in range(f):
    for j in range(c):
      M[i][j]=int(input("Valor "))

def mostrar(M,f,c,):
  for i in range(f):
    for j in range(c):
      print('| ',M[i][j],end=" ")
    print('|')

## FUNCION PARA HALLAR LA SUMA DE LAS COLUMNAS
def sumaDeCol(M,f,c):
  for i in range(c):
    sum = 0
    for j in range(f):
      sum += M[j][i]
    print(f'la suma de la columa {i+1} es {sum}')

## 4. FUNCION PARA HALLAR EL PRODUCTO DE LA DIAGONAL PRINCIPAL
def multDeDiag(M,f,c):
  mult = 1
  for i in range(f):
    for j in range(c):
      if i == j:
        mult = mult*M[j][i]
  return mult

## 5. SUMA DE LA DIAGONAL PRINCIPAL
def sumDiagP(M,f):
  sump = 0
  for i in range(f):
        sump = sump+M[i][i]
  return sump

## 6. SUMA DE LA DIAGONAL SECUNDARIA
def sumDiagS(M,f):
  sum = 0
  for i in range(f):
        sum = sum + M[i][f-1-i]
  return sum


M=[]
f=int(input("Cantidad de filas? "))
c=int(input("Cantidad de columnas? "))
    
## Ingresar y mostrar Matrices
crearMatriz(M,f,c)  
ingresar(M,f,c)
print("Matriz:")
mostrar(M,f,c,)

## Suma de las columanas
print('Sumas de las columnas')
sumaDeCol(M,f,c)

### SOLO FUNCIONA CUANDO LA MATRIZ ES CUADRADA

## Producto de la diagonal principal
print(f'El producto de la diagonal principal es:')
print(multDeDiag(M,f,c))

## Suma de la diagonal principal
print(f'La suma de la diagnoal secundaria es:')
print(sumDiagP(M,f))

## Suma de la diagonal secundaria
print(f'La suma de la diagnoal secundaria es:')
print(sumDiagS(M,f))
