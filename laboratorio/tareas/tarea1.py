def crearMatriz(M,orden):
  for i in range(orden):
    M.append([0]*orden)

def ingresar(M,orden):
  for i in range(orden):
    for j in range(orden):
      M[i][j]=int(input("Valor: "))

def mostrar(M,orden):
  for i in range(orden):
    for j in range(orden):
      print(M[i][j],end=" ")
    print()

def interDiag(M,orden):
  for i in range(orden):
    aux = M[i][i] 
    M[i][i] = M[i][orden-1]
    M[i][orden-1-i] = aux
  mostrar(M, orden)

M = []
orden = int(input("Orden de las matriz?: "))
crearMatriz(M,orden)
ingresar(M,orden)
mostrar(M,orden)

print('Intercambio de diagonales')
interDiag(M,orden)
