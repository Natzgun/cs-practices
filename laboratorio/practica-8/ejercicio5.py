def separador(n):
  for i in range(len(n)):
    if i%2 == 0:
      frontend.write(n[i])
    elif i%2 == 1:
      backend.write(n[i])

rayuela = open('./practica-8/rayuela.txt', 'r')
frontend = open('./practica-8/frontend.txt', 'a')
backend = open('./practica-8/backend.txt', 'a')

### readlines() en una varible es como una lista de Strings
n = rayuela.readlines()
separador(n)
