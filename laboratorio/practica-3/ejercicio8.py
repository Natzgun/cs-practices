#8. Se introducen 5 números por teclado, y se desea calcular cuántos son mayores que 0, cuantos son menores a 0 y cuantos son iguales a 0

for i in range(5):
  valor = int(input('Ingrese valor: '))
  if valor > 0:
    print(f'El valor {valor} es mayor a 0')
  elif valor < 0:
    print(f'El valor {valor} es menor a 0')
  else:
    print('El valor es igual a cero FIN DEL BUCLE')
