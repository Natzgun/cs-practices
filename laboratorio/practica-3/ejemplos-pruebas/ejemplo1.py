#8. Se introducen números por teclado hasta que se ingresa el valor 0, y se desea calcular cuántos son positivos y cuántos son negativos.

objetivo = int(input('Escoge un N°: '))

while objetivo != 0:
    if objetivo > 0:
      print(f'El valor {objetivo} es mayor a 0')
    elif objetivo < 0:
      print(f'El valor {objetivo} es menor a 0')
    objetivo = int(input('Escoge otro N° '))
print(objetivo)
