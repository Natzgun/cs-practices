
# 10. Dado un valor de x calcular el valor de y según la siguiente función:

from math import *
valorX = int(input('Ingresa un valor para X: '))

if valorX <= 5:
  y = 3*valorX+36
elif 5 < valorX and  valorX <= 8:
  y = pow(valorX,2)-10
elif valorX%9 == 0:
  y = pow(valorX,3)+6
else:
  y = sqrt(valorX)
print(f'El valor de Y es {y}')
