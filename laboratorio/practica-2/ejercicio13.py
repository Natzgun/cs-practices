"""""13. Diseñe un programa que lea un número entero (positivo o negativo) y determine si tiene 1, 2, 3,
4 o más de 4 cifras imprimiendo lo que corresponda. """""

digito = int(input('Ingrese un N°: '))
num = abs(digito)
contador = 0

if num > 0:
  while num > 0:
    num = num//10
    contador+=1
  print(f'El numero que elegiste tiene: {contador} digitos')
elif num == 0:
  print('El numero que elegiste tiene 1 digito')
