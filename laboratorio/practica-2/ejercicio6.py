

#6. Ingresar tres números y verificar si el primero es divisor común de los otros dos.
num1 = int(input('Ingrese primer numero: '))
num2 = int(input('Ingrese segundo numero: '))
num3 = int(input('Ingrese tercer numero: '))

if num2%num1 == 0 and num3%num1 == 0:
  print(f'El {num1} es divisor comun de {num2} y {num3}')
else:
  print(f'El {num1} no es divisor comun de ambos')
