""" Determinar si un número es par o impar. """
num = int(input('Escribe un num: '))

if num%2 == 0:
  print(f'El numero {num} es par')
elif num%2 == 1:
  print(f'El numero {num} es impar')
