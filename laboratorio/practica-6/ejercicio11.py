
def esta(L,x):
  tam = len(L)
  for i in range(tam):
    if L[i] == x:
      return True
  return False

L = [1,2,3,4,5,6]
x = int(input('Valor a buscar (x): '))

if esta(L,x) == True:
  print(f'{x} si esta en la lista')
else:
  print(f'{x} no esta en la lista')
