""" 9. Hallar la sumatoria de los valores que se encuentran en las posiciones pares de la lista
(desde el punto de vista del usuario). """
def lista(L, n):
  while n != 0:
    L.append(n)
    n = int(input('Otro numero: '))
  print(L)
def sumPosPares(L, n):
  suma = 0
  for i in range(1, n,2):
      suma = suma + L[i]
  return suma

L = []
n = int(input('Numero: '))
lista(L,n)

num = len(L)
print(f'La suma de las posiciones pares es {sumPosPares(L,num)}')