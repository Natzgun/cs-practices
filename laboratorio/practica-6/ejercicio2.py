# Ejercicio 2 
"""Ingresar N valores a una lista y mostrar el contenido. Realizar por medio de los tres
métodos revisados en clase."""


# METODO 1
def ingresar(L,n):
  for i in range(n):
    num = int(input('Numero: '))
    L.append(num)
def mostrar(L):
  print(L)

n = int(input('Ingrese el rango: '))
L = []
ingresar(L,n)
mostrar(L)

#METODO 2
# insert pide que ingresemos 2 parametros (posicion, valor)
def ingresar2(L,n):
  for i in range(n):
    num = int(input('Numero: '))
    L.insert(i, num)
def mostrar2(L):
  print(L)

r = int(input('Ingrese el rango: '))
lista = []
ingresar2(lista,r)
mostrar2(lista)

#METODO 3
def ingresar3(L,n):
  for i in range(n):
    num = int(input('Numero: '))
    L[i] = num
def mostrar3(L):
  print(L)

ran = int(input('Ingrese el rango: '))
# Aqui se crea una lista con tantos ceros como se espeficique en el rango
li = [0]*ran
ingresar3(li,ran)
mostrar3(li)


