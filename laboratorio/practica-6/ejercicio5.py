# 5
def ingresar (L):
  num= int(input('Numero: '))
  while num != -1:
    L.append(num)
    num= int(input('Numero: '))

def mostrar(L):
  for i in L:
    print(i, end=' ')

""" def esPrimo(num):
  for i in range(2, num//2+1):
    if num%i == 0:
      return False
  return True """

def esPrimo(num):
  cont = 0
  for i in range(1,num+1):
    if num%i == 0:
      cont = cont +1
  if cont == 2:
    return True
  return False


def cantPrimo(L):
  c = 0
  for i in L:
    if esPrimo(i) == True:
      c = c + 1
  return c
 
L = []
ingresar(L)
print('La lista contiene: ')
mostrar(L)
print(f'\nCantidad de numeros primos {cantPrimo(L)}')