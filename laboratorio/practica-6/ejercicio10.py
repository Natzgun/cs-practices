# EJERCICIO 10
from math import *
def ingresar(L, n):
  for i in range(n):
    num = int(input(f'Valor {str(i + 1)} ? '))
    L.append(num)

def mediaArit(L, n):
  s = 0
  for i in L:
    s = s + i
  return s/n 

def varianza(L,n):
  media = mediaArit(L,n)
  s = 0
  for i in L:
    s = s + pow(i - media, 2)
  return s/n

def desvEstandar(L, n):
  desv = sqrt(varianza(L,n))
  return desv

n = int(input('N: '))
L = []
ingresar(L,n)
print(f'Media Aritmetica = {mediaArit(L,n)}')
print(f'Varianza = {varianza(L,n)}')
print(f'Desviacion estandar = {desvEstandar(L,n)}')
