def factorial(n):
  if n == 0:
    return 1
  else:
    return n * factorial(n-1)

n = int(input('Numero: '))
print(f'El factorial de {n} es ==> {factorial(n)}')

"""
fact(5) = 5 * fact(4)
fact(4) = 4 * fact(3)
fact(3) = 3 * fact(2)
fact(2) = 2 * fact(1)
fact(1) = 1 * fact(0)
fact(0) = 1
"""